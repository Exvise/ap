import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { FormControl, FormGroup, NgForm, Validators  } from "@angular/forms";
import { JwtHelperService } from '@auth0/angular-jwt';
import { User } from '../models/user';
import { LoginModel } from '../models/loginModel';
import { UserService } from '../services/user.service';
import { LoginService } from '../services/login.service';
import { Md5 } from 'ts-md5/dist/md5';
import { UniqueUsernameValidator } from '../directives/username.directive';
import { CustomValidators } from './custom-validators';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  invalidLogin: boolean;
  invalidRegister: boolean;
  loginModel: LoginModel = { username: '', password: '', confirmPassword: '' };
  registerForm: FormGroup;

  constructor(
    private router: Router, 
    private http: HttpClient, 
    private userService: UserService,
    private loginService: LoginService,
    private jwtHelper: JwtHelperService,
    private usernameValidator: UniqueUsernameValidator) { }

  async ngOnInit(): Promise<void> {
    this.registerForm = new FormGroup(
    {
      username: new FormControl(this.loginModel.username, {
        asyncValidators: [
          this.usernameValidator.validate.bind(this.usernameValidator)
        ],
        updateOn: 'blur'
      }),
      password: new FormControl(this.loginModel.password, [
        Validators.compose([Validators.required]),
        CustomValidators.patternValidator(/\d/, {
          hasNumber: true
        }),
        CustomValidators.patternValidator(/[A-Z]/, {
          hasCapitalCase: true
        }),
        CustomValidators.patternValidator(/[a-z]/, {
          hasLowerCase: true
        }),
        Validators.minLength(3)
      ],
      ),
      confirmPassword: new FormControl(this.loginModel.confirmPassword, [
        Validators.required
      ])
    },
    {
       // check whether our password and confirm password match
       validators: CustomValidators.passwordMatchValidator
    });

    const signUpButton = document.getElementById('signUp');
    const signInButton = document.getElementById('signIn');
    const container = document.getElementById('container');

    signUpButton.addEventListener('click', () => {
        container.classList.add("right-panel-active");
    });

    signInButton.addEventListener('click', () => {
        container.classList.remove("right-panel-active");
    });
  }

  get username() { return this.registerForm.get('username'); }
  get password() { return this.registerForm.get('password'); }
  get confirmPassword() { return this.registerForm.get('confirmPassword'); }

  public login = (form: NgForm) => {
    form.value.password = Md5.hashStr(form.value.password);
    const credentials = JSON.stringify(form.value);
    this.loginService.signin(credentials)
      .subscribe(response => {
        this.invalidLogin = response;
        this.router.navigate(["/projects"]);
      }, err => {
        this.invalidLogin = true;
      });
  }

  public register = (form: NgForm) => {
    if(form.value.password != form.value.confirmPassword){
      return;
    }
    form.value.password = Md5.hashStr(form.value.password);
    const credentials = JSON.stringify(form.value);
    
    this.loginService.signup(form.value as User)
      .subscribe(user => {
        this.invalidRegister = false;

        this.loginService.signin(credentials)
        .subscribe(response => {
          this.router.navigate(["/projects"]);
        }, err => {
          this.router.navigate(["/login"]);
        });
      }, err => {
        this.invalidRegister = true;
      });
  }

}

export function ConfirmedValidator(controlName: string, matchingControlName: string){
  return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];
      if (matchingControl.errors && !matchingControl.errors.confirmedValidator) {
          return;
      }
      if (control.value !== matchingControl.value) {
          matchingControl.setErrors({ confirmedValidator: true });
      } else {
          matchingControl.setErrors(null);
      }
  }
}