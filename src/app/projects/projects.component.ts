import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Project } from '../models/project';
import { ProjectService } from '../services/project.service';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css']
})
export class ProjectsComponent implements OnInit {

  projects: Project[];

  constructor(
    private route: ActivatedRoute,
    private projectService: ProjectService
  ) {}

  ngOnInit(): void {
    this.getProjects();
  }

  getProjects(): void {
    const id: number =+ localStorage.getItem("userId");
    this.projectService.getProjectsByUserId(id)
      .subscribe(projects => this.projects = projects);
  }

  addProject(name: string): void {
    name = name.trim();
    const userId: number =+ localStorage.getItem("userId");
    if (!name) { return; }
    this.projectService.addProject({name: name, adminId: userId} as Project)
      .subscribe(project => {
        this.projects.push(project);
      });
  }

  delete(project: Project): void {
    this.projects = this.projects.filter(p => p !== project);
    this.projectService.deleteProject(project).subscribe();
  }
}
