export interface Message {
    text: string;
    type: MessageType;
}

export enum MessageType {
    success = 'success',
    info = 'info',
    warning = 'warning',
    danger = 'danger',
    primary = 'primary'
}