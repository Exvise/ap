export interface UserProject {
    id: number;
    userId: number;
    projectId: number;
    roleId: number;
  }