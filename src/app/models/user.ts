import { Role } from './role';

export interface User {
    id: number;
    username: string;
    password: string;
    photoBase64: string;
    refreshToken: string;
    refreshTokenExpiryTime: Date;
  }