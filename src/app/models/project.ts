export interface Project {
    id: number;
    name: string;
    adminId: number;
  }