import { Injectable } from '@angular/core';
import { User } from '../models/user';
import { Observable, of, Subject } from 'rxjs';
import { MessageService } from './message.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, delay, map, tap } from 'rxjs/operators';
import { MessageType } from '../models/message';


const ALTER_EGOS = ['Eric'];

@Injectable({
  providedIn: 'root',
})
export class UserService {
  private usersUrl = 'http://localhost:8085/userapi';  // URL to web api

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(
    private http: HttpClient,
    private messageService: MessageService) { }

  /** POST: add a new user to the server */
  addUser(user: User): Observable<User> {
    return this.http.post<User>(this.usersUrl, user, this.httpOptions).pipe(
      tap((newUser: User) => this.messageService.log(`added user id=${newUser.id}`, MessageType.success)),
      catchError(this.messageService.handleError<User>('addUser'))
    );
  }

  /** POST: add a new user photo to the server */
  addPhoto(photo: File, id: number | string): Observable<string>{
    const fd = new FormData();
    fd.set('image', photo, photo.name);

    return this.http.post<string>(`${this.usersUrl}/${id}/photo`, fd).pipe(
      tap(() => this.messageService.log(`added photo of user id=${id}`, MessageType.success)),
      catchError(this.messageService.handleError<string>('addPhoto'))
    );
  }

  /** POST: delete a user photo to the server */
  deletePhoto(id: number | string): Observable<string>{
    return this.http.delete<string>(`${this.usersUrl}/${id}/photo`).pipe(
      tap(() => this.messageService.log(`deleted photo of user id=${id}`, MessageType.success)),
      catchError(this.messageService.handleError<string>('deletePhoto'))
    );
  }

  /** GET users from the server */
  getUsersFromProject(projectId: number): Observable<User[]> {
    const url = `http://localhost:8085/projectapi/${projectId}/user`; //${this.usersUrl}/${projectId}     
    return this.http.get<User[]>(url).pipe(
      tap(x => x.length ?
        this.messageService.log(`fetched users from project with id = "${projectId}"`, MessageType.success) :
        this.messageService.log(`project with id = "${projectId}" doesn't have users`, MessageType.info)),
      catchError(this.messageService.handleError<User[]>(`getUsersFromProject id=${projectId}`))
    );
  }

  /** GET users from the server */
  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(this.usersUrl)
      .pipe(
        tap(x => x.length ?
          this.messageService.log(`fetched all users`, MessageType.success) :
          this.messageService.log(`no users`, MessageType.info)),
        catchError(this.messageService.handleError<User[]>('getUsers', []))
      );
  }

  /** GET user by id. Will 404 if id not found */
  getUser(id: number): Observable<User> {
    const url = `${this.usersUrl}/${id}`;
    return this.http.get<User>(url).pipe(
      tap(_ => this.messageService.log(`fetched user id=${id}`, MessageType.success)),
      catchError(this.messageService.handleError<User>(`getUser id=${id}`))
    );
  }
  
  /** GET user by id. Will 404 if id not found */
  getUserByName(username: string): Observable<User> {
    const url = `${this.usersUrl}/name/${username}`;
    return this.http.get<User>(url).pipe(
      tap(_ => this.messageService.log(`fetched user username=${username}`, MessageType.success)),
      catchError(this.messageService.handleError<User>(`getUserByName username=${username}`))
    );
  }

  /** PUT: update the user on the server */
  updateUser(user: User): Observable<any> {
    return this.http.put(this.usersUrl, user, this.httpOptions).pipe(
      tap(_ => this.messageService.log(`updated user id=${user.id}`, MessageType.success)),
      catchError(this.messageService.handleError<any>('updateUser'))
    );
  }

  /** DELETE: delete the user from the server */
  deleteUser(user: User | number): Observable<User> {
    const id = typeof user === 'number' ? user : user.id;
    const url = `${this.usersUrl}/${id}`;

    return this.http.delete<User>(url, this.httpOptions).pipe(
      tap(_ => this.messageService.log(`deleted user id=${id}`, MessageType.success)),
      catchError(this.messageService.handleError<User>('deleteUser'))
    );
  }

  isUsernameTaken(username: string): Observable<boolean> {
    return this.getUsers().pipe(
      map(users => {
        return !(users.filter(c => c.username == username).length == 0);
      })
    );
  }

  /* GET users whose name contains search term */
  searchUsers(term: string): Observable<User[]> {
    console.log(term);
    if (!term.trim()) {
      // if not search term, return empty user array.
      return of([]);
    }
    return this.http.get<User[]>(`${this.usersUrl}/search/${term}`).pipe(
      tap(x => x.length ?
        this.messageService.log(`fetched users matching "${term}"`, MessageType.success) :
        this.messageService.log(`no users matching "${term}"`, MessageType.info)),
      catchError(this.messageService.handleError<User[]>('searchUsers', []))
    );
  }
}
