import { Injectable } from '@angular/core';
import { User } from '../models/user';
import { Observable, of } from 'rxjs';
import { MessageService } from './message.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { MessageType } from '../models/message';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private loginUrl = 'http://localhost:8085/identityapi';  // URL to web api

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(
    private http: HttpClient,
    private messageService: MessageService) { }

  signin(credentials: string) {
    return this.http.post<any>(`${this.loginUrl}/login`, credentials, this.httpOptions)
      .pipe(map(response => {      
          console.log(response);
          const token = (<any>response).token;
          const refreshToken = (<any>response).refreshToken;
          const userId = (<any>response).userId;
          localStorage.setItem("jwt", token);
          localStorage.setItem("refreshToken", refreshToken);
          localStorage.setItem("userId", userId);
          return  false;
      }, 
      err => {
        return true;
      }
    ));
  }

  signup(user: User){
    return this.http.post<User>(`${this.loginUrl}/signup`, user, this.httpOptions)
    .pipe(
      tap((newUser: User) => this.messageService.log(`added user id=${newUser.id}`, MessageType.success)),
      catchError(this.messageService.handleError<User>('addUser'))
    );
  }
}
