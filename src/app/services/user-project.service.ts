import { Injectable } from '@angular/core';
import { Project } from '../models/project';
import { Observable, of } from 'rxjs';
import { MessageService } from './message.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

import { UserProject } from '../models/user-project';
import { MessageType } from '../models/message';

@Injectable({
  providedIn: 'root'
})
export class UserProjectService {
  private userprojectUrl = 'http://localhost:8085/userprojectapi';  // URL to web api

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(
    private http: HttpClient,
    private messageService: MessageService) { }

  /** POST: add a user in a project to the server */
  addUserInProject(userProject: UserProject): Observable<UserProject> {
    return this.http.post<UserProject>(this.userprojectUrl, userProject, this.httpOptions).pipe(
      tap((newUserProject: UserProject) => this.messageService.log(`added user with id=${newUserProject.userId} in project with id=${newUserProject.projectId}`, MessageType.success)),
      catchError(this.messageService.handleError<UserProject>('addUserInProject'))
    );
  }

  /** GET userProject by projectId and userId from the server */
  getUserInProject(projectId: number, userId: number): Observable<UserProject> {
    return this.http.get<UserProject>(this.userprojectUrl + '/' + projectId + '/' + userId).pipe(
      tap((fetchedUserProject: UserProject) => this.messageService.log(`fetched userProject id=${fetchedUserProject.id}`, MessageType.success)),
      catchError(this.messageService.handleError<UserProject>(`getUserInProject userId=${userId}, projectId=${projectId}`))
    );
  } 
  
  /** PUT: update the userProject on the server */
  updateUserInProject(userProject: UserProject): Observable<UserProject> {
    return this.http.put<UserProject>(this.userprojectUrl, userProject, this.httpOptions).pipe(
      tap((updatedUserProject: UserProject) => this.messageService.log(`updated userProject id=${updatedUserProject.id}`, MessageType.success)),
      catchError(this.messageService.handleError<UserProject>('updateUserInProject'))
    );
  }

  /** DELETE: delete the userProject from the server */
  deleteUserFromProject(userProject: UserProject): Observable<UserProject>{
    return this.http.delete<UserProject>(this.userprojectUrl + '/' + userProject.projectId + '/' + userProject.userId, this.httpOptions).pipe(
      tap(_ => this.messageService.log(`deleted user with id=${userProject.userId} from project with id=${userProject.projectId}`, MessageType.success)),
      catchError(this.messageService.handleError<UserProject>('deleteUserFromProject'))
    );
  }
}