import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap, take } from 'rxjs/operators';

import { ProjectService } from './project.service';
import { Project } from '../models/project';
import { UserProjectService } from './user-project.service';

@Injectable({
  providedIn: 'root',
})
export class ProjectDetailResolverService implements Resolve<Project> {
  constructor(
    private projectService: ProjectService, 
    private userProjectService: UserProjectService, 
    private router: Router) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Project> | Observable<never> {
    const userId: number =+ localStorage.getItem("userId");
    const id =+ route.paramMap.get('id');
    this.userProjectService.getUserInProject(id, userId)
      .subscribe(userProject => {
        if(userProject == null){
          this.router.navigate(['/projects']);
          return EMPTY;
        }
      });

    return this.projectService.getProject(id).pipe(
      take(1),
      mergeMap(crisis => {
        if (crisis) {
          return of(crisis);
        } else { // id not found
          this.router.navigate(['/projects']);
          return EMPTY;
        }
      })
    );
  }
}
