import { Injectable } from '@angular/core';
import { Project } from '../models/project';
import { Observable, of } from 'rxjs';
import { MessageService } from './message.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

import { UserProject } from '../models/user-project';
import { MessageType } from '../models/message';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {
  private projectsUrl = 'http://localhost:8085/projectapi';  // URL to web api

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(
    private http: HttpClient,
    private messageService: MessageService) { }

  /** POST: add a new project to the server */
  addProject(project: Project): Observable<Project> {
    return this.http.post<Project>('http://localhost:8085/projectapi', project, this.httpOptions).pipe(
      tap((newProject: Project) => this.messageService.log(`added project id=${newProject.id}`, MessageType.success)),
      catchError(this.messageService.handleError<Project>('addProject'))
    );
  }

  /** GET projects from the server */
  getProjects(): Observable<Project[]> {
    return this.http.get<Project[]>(this.projectsUrl)
      .pipe(
        tap(_ => this.messageService.log('fetched projects', MessageType.success)),
        catchError(this.messageService.handleError<Project[]>('getProjects', []))
      );
  }

  /** GET project by id. Will 404 if id not found */
  getProject(id: number): Observable<Project> {
    const url = `${this.projectsUrl}/${id}`;
    return this.http.get<Project>(url).pipe(
      tap(_ => this.messageService.log(`fetched project id=${id}`, MessageType.success)),
      catchError(this.messageService.handleError<Project>(`getProject id=${id}`))
    );
  }
  
  /** GET projects by userId. Will 404 if id not found */
  getProjectsByUserId(userId: number): Observable<Project[]> {
    const url = `http://localhost:8085/user/${userId}/projects`;
    return this.http.get<Project[]>(url).pipe(
      tap(_ => this.messageService.log(`fetched projects by userId=${userId}`, MessageType.success)),
      catchError(this.messageService.handleError<Project[]>('getProjectsByUserId', []))
    );
  }

  /** PUT: update the project on the server */
  updateProject(project: Project): Observable<any> {
    return this.http.put(this.projectsUrl, project, this.httpOptions).pipe(
      tap(_ => this.messageService.log(`updated project id=${project.id}`, MessageType.success)),
      catchError(this.messageService.handleError<any>('updateProject'))
    );
  }

  /** DELETE: delete the project from the server */
  deleteProject(project: Project | number): Observable<Project> {
    const id = typeof project === 'number' ? project : project.id;
    const url = `${this.projectsUrl}/${id}`;

    return this.http.delete<Project>(url, this.httpOptions).pipe(
      tap(_ => this.messageService.log(`deleted project id=${id}`, MessageType.success)),
      catchError(this.messageService.handleError<Project>('deleteProject'))
    );
  }
}
