import { Injectable } from '@angular/core';
import { Role } from '../models/role';
import { Observable, of } from 'rxjs';
import { MessageService } from './message.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { MessageType } from '../models/message';

@Injectable({
  providedIn: 'root'
})
export class RoleService {
  private rolesUrl = 'http://localhost:8085/roleapi';  // URL to web api

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(
    private http: HttpClient,
    private messageService: MessageService) { }

  /** GET roles from the server */
  getRoles(): Observable<Role[]> {
    return this.http.get<Role[]>(this.rolesUrl)
      .pipe(
        tap(x => x.length ?
          this.messageService.log(`fetched all roles`, MessageType.success) :
          this.messageService.log(`no roles`, MessageType.info)),
        catchError(this.messageService.handleError<Role[]>('getRoles', []))
      );
  }

  /** GET role by id. Will 404 if id not found */
  getRole(id: number): Observable<Role> {
    const url = `${this.rolesUrl}/${id}`;
    return this.http.get<Role>(url).pipe(
      tap(_ => this.messageService.log(`fetched role id=${id}`, MessageType.success)),
      catchError(this.messageService.handleError<Role>(`getRole id=${id}`))
    );
  }
  
  /** GET role in project by userId and projectId. Will 404 if id not found */
  getRoleInProject(userId: number, projectId: number): Observable<Role> {
    const url = `${this.rolesUrl}/${userId}/${projectId}`;
    return this.http.get<Role>(url).pipe(
      tap(_ => this.messageService.log(`fetched user role with userId = ${userId} in project with projectId = ${projectId}`, MessageType.success)),
      catchError(this.messageService.handleError<Role>(`getRoleInProject userId = ${userId}, projectId = ${projectId}`))
    );
  }
}
