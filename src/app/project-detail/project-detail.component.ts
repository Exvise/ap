import { Component, OnInit, Input } from '@angular/core';
import { Role } from '../models/role';
import { UserProject } from '../models/user-project';
import { Project } from '../models/project';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { HttpHeaders } from '@angular/common/http';
import { NgxImageCompressService } from 'ngx-image-compress';
import {
   debounceTime, distinctUntilChanged, switchMap
 } from 'rxjs/operators';

import { UserProjectService } from '../services/user-project.service';
import { ProjectService } from '../services/project.service';
import { RoleService } from '../services/role.service';
import { UserService } from '../services/user.service';
import { User } from '../models/user';
import { Subject } from 'rxjs';


@Component({
  selector: 'app-project-detail',
  templateUrl: './project-detail.component.html',
  styleUrls: ['./project-detail.component.css']
})
export class ProjectDetailComponent implements OnInit {
  @Input() project: Project;
  users: User[];
  selectedUser: User = null;
  roles: Role[];
  role: number;
  selectedRole: number;
  selectedRoleNewUser: number;
  projects: Project[];
  photoBase64: string = "./assets/avatar-default.png";
  selectedFile: File = null;
  localUrl: any;
  fileName: any;

  foundUsers: User[];
  private searchTerms = new Subject<string>();

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(
    private route: ActivatedRoute,
    private projectService: ProjectService,
    private userProjectService: UserProjectService,
    private roleService: RoleService,
    private userService: UserService,
    private location: Location,
    private imageCompress: NgxImageCompressService
  ) {}

  ngOnInit(): void {
    this.route.data
      .subscribe((data: { project: Project }) => {
        this.project = data.project;
      });
      
    this.getRoles();
    this.getRole();
    this.getUsersFromProject();

    this.searchTerms.pipe(
      // wait 300ms after each keystroke before considering the term
      debounceTime(300),

      // ignore new term if same as previous term
      distinctUntilChanged(),

      // switch to new search observable each time the term changes
      switchMap((term: string) => this.userService.searchUsers(term))
    ).subscribe(users =>{
      for( var i=users.length - 1; i>=0; i--){
        for( var j=0; j<this.users.length; j++){
            if(users[i] && (users[i].username === this.users[j].username)){
              users.splice(i, 1);
           }
         }
      }
      this.foundUsers = users;
    });
  }

  // Push a search term into the observable stream.
  search(term: any): void {
    this.searchTerms.next(term);
  }

  onFileSelected(event) {
    var reader = new FileReader();
      reader.onload = (event: any) => {
        this.localUrl = event.target.result;
      }
    reader.readAsDataURL(event.target.files[0]);
    this.fileName = this.selectedFile['name'];
  }

  onChangeRole(newRole) {
    this.selectedRole = newRole;
  }
  
  onChangeRoleNewUser(newRole) {
    this.selectedRoleNewUser = newRole;
  }

  onUserChange(user: User){
    this.selectedUser = user;
    this.roleService.getRoleInProject(user.id, this.project.id)
      .subscribe(role => this.selectedRole = role.id);
  }

  addUserInProject(username: string): void{
    if(this.selectedRoleNewUser == null || username == "")
      return;
    this.userService.getUserByName(username)
      .subscribe(user => 
        {
          let userProject: UserProject = {id: 0, projectId: this.project.id, roleId: this.selectedRoleNewUser, userId: user.id};
          
          this.userProjectService.addUserInProject(userProject)
            .subscribe(userProject => {
              this.users.push(user);
              this.selectedRoleNewUser = null;
            },
            err => console.log(err));
        });
  }
  
  getRole(): void {
    const userId: number =+ localStorage.getItem("userId");
    this.roleService.getRoleInProject(userId, this.project.id)
      .subscribe(role => this.selectedRole = role.id);
  }

  getRoles(): void {
    this.roleService.getRoles()
      .subscribe(roles => this.roles = roles);
  }
  
  getUsersFromProject(): void {
    this.userService.getUsersFromProject(this.project.id)
      .subscribe(users => this.users = users);
  }
  
  getProject(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.projectService.getProject(id)
      .subscribe(project => this.project = project);
  }

  delete(user: User): void {
    let userProject: UserProject = {id: 0, projectId: this.project.id, roleId: this.selectedRoleNewUser, userId: user.id};
    this.userProjectService.deleteUserFromProject(userProject)
      .subscribe(userProject => {
        this.users = this.users.filter(u => u !== user);
        this.selectedUser = null;
      });
  }

  saveProject(): void {
    this.projectService.updateProject(this.project)
      .subscribe(() => this.goBack());
  }

  saveUserInProject(): void {
    this.userProjectService.getUserInProject(this.project.id, this.selectedUser.id)
      .subscribe(fetchedUserProject => {
        fetchedUserProject.roleId = this.selectedRole;
        this.userProjectService.updateUserInProject(fetchedUserProject)
          .subscribe(userProject => {
            this.getUsersFromProject();
            this.selectedUser = null;
          });
      });
  }
  
  goBack(): void {
    this.location.back();
  }
}

