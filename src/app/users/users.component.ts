import { Component, OnInit } from '@angular/core';

import { User } from '../models/user';
import { Role } from '../models/role';
import { UserService } from '../services/user.service';
import { RoleService } from '../services/role.service';
import { MessageService } from '../services/message.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  users: User[];
  roles: Role[];
  selectedRole: string;
  reverse: boolean = true;

  constructor(
    private messageService: MessageService,
    private userService: UserService,
    private roleService: RoleService) { }

  ngOnInit() {
    this.getUsers();
    
    this.getRoles();
  }

  getUsers(): void {
    this.userService.getUsers()
    .subscribe(users => this.users = users);
  }

  getRoles(): void {
    this.roleService.getRoles()
    .subscribe(roles => this.roles = roles);
  }

  add(name: string, roleId: number): void {
    name = name.trim();
    roleId = Number(roleId);
    if (!name) { return; }
    this.userService.addUser({username: name, password: '123qq'} as User)
      .subscribe(user => {
        this.users.push(user);
      });
  }

  delete(user: User): void {
    this.users = this.users.filter(u => u !== user);
    this.userService.deleteUser(user).subscribe();
  }

  sort(): void {
    this.users = this.users.sort((t1, t2) => {
      const name1 = t1.username.toLowerCase();
      const name2 = t2.username.toLowerCase();
        if (name1 > name2) { return this.reverse ? 1 : -1; }
        if (name1 < name2) { return this.reverse ? -1 : 1; }
      return 0;
    });
    this.reverse = !this.reverse;
  }
}