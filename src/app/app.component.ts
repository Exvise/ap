import { ContentObserver } from '@angular/cdk/observers';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { User } from './models/user';
import { UserService } from './services/user.service';
import { AuthGuard } from './services/auth-guard.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  photoBase64: string = "./assets/avatar-default.png";
  user: User;
  
  constructor(
    private jwtHelper: JwtHelperService, 
    private userService: UserService,
    private authService: AuthGuard,
    private router: Router) {
  }

  ngOnInit(): void {
    const id =+ localStorage.getItem("userId");
    this.userService.getUser(id)
      .subscribe(user => {  
        this.user = user;
        if(this.user.photoBase64)
          this.photoBase64 = this.user.photoBase64;
        
      });
  }

  isUserAuthenticated() {
    const token = localStorage.getItem("jwt");
    if (token && !this.jwtHelper.isTokenExpired(token)) {
      return true;
    }
    else {
      return false;
    }
  }

  public logOut = () => {
    localStorage.removeItem("jwt");
    localStorage.removeItem("refreshToken");
    this.router.navigate(["login"]);
  }
}