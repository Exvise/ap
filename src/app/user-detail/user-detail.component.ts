import { Component, OnInit, Input } from '@angular/core';
import { User } from '../models/user';
import { Role } from '../models/role';
import { Project } from '../models/project';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { HttpHeaders } from '@angular/common/http';
import { NgxImageCompressService } from 'ngx-image-compress';

import { UserService } from '../services/user.service';
import { RoleService } from '../services/role.service';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit {
  @Input() user: User;
  selectedRole: number;
  roles: Role[];
  projects: Project[];
  photoBase64: string = "./assets/avatar-default.png";
  selectedFile: File = null;
  localUrl: any;
  fileName: any;

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(
    private route: ActivatedRoute,
    private userService: UserService,
    private roleService: RoleService,
    private location: Location,
    private imageCompress: NgxImageCompressService
  ) {}

  ngOnInit(): void {
    this.route.data
      .subscribe((data: { user: User }) => {
        this.user = data.user;
        if(data.user.photoBase64)
          this.photoBase64 = data.user.photoBase64;
      });
  }

  onUpload() {
    const id = +this.route.snapshot.paramMap.get('id');
    this.imageCompress.compressFile(this.localUrl, -1, 30, 30).then(
      result => {
        var arr = result.split(','),
            bstr = atob(arr[1]), 
            n = bstr.length, 
            u8arr = new Uint8Array(n);
        while(n--){
            u8arr[n] = bstr.charCodeAt(n);
        }
        const file = new File([u8arr], this.fileName, { type: 'image/jpeg' });
        this.userService.addPhoto(file, id)
          .subscribe(photoBase64 => this.photoBase64 =  photoBase64);
      }
    );
  }

  onFileSelected(event) {
    var reader = new FileReader();
      reader.onload = (event: any) => {
        this.localUrl = event.target.result;
      }
    reader.readAsDataURL(event.target.files[0]);
    this.selectedFile = event.target.files[0];
    this.fileName = this.selectedFile['name'];
  }

  onChangeObj(newObj) {
    this.selectedRole = newObj;
  }

  deletePhoto(){
    const id = +this.route.snapshot.paramMap.get('id');
    this.userService.deletePhoto(id)
      .subscribe(photoBase64 => this.photoBase64 = "./assets/avatar-default.png");
  }
  
  getUser(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.userService.getUser(id)
      .subscribe(user => this.user = user);
  }
  
  save(): void {
    this.userService.updateUser(this.user)
      .subscribe(() => this.goBack());
  }
  
  goBack(): void {
    this.location.back();
  }
}
